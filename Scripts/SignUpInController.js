﻿app.controller('signUpInCtrl', function ($scope, $mdDialog, $mdMedia, $timeout, $sce, $mdSidenav) {

    $scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');
    function buildToggler(componentId) {
        return function () {
            $mdSidenav(componentId).toggle();
        }
    }

    //Sign up func
    $scope.signup = function (ev) {
        var email = $scope.signup.email;
        var password = $scope.signup.password;
        if (password === $scope.signup.confirmPassword) {
            firebase.auth().createUserWithEmailAndPassword($scope.signup.email, $scope.signup.password).then(
                function (success) {
                    if (success.uid != undefined) {
                        var updates = {};
                        updates['/users/' + success.uid] = $scope.userinfo;

                        var result = database.ref().update(updates).catch(function (error) { });
                    }
                },
                function (error) {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title(error.code)
                        .textContent(error.message)
                        .ariaLabel('Error')
                        .ok('Got it!')
                        .targetEvent(ev)
                   );
                });
        } else {
            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Confirm password not match !')
                .textContent('Check your password and confirm password field again.')
                .ariaLabel('Alert')
                .ok('Got it!')
                .targetEvent(ev)
            );
        }
    };
    //Login func
    $scope.login = function (ev) {
        var email = $scope.login.email;
        var password = $scope.login.password;

        var loginResult = firebase.auth().signInWithEmailAndPassword(email, password)
            .catch(function (error) {
                $mdDialog.show(
                       $mdDialog.alert()
                       .parent(angular.element(document.querySelector('#popupContainer')))
                       .clickOutsideToClose(true)
                       .title(error.code)
                       .textContent(error.message)
                       .ariaLabel('Error')
                       .ok('Got it!')
                       .targetEvent(ev)
                  );
                return false;
            })
            .then(function (success) {
                if (success !== false) {
                    $mdDialog.show(
                            $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('Login Successfully')
                            .textContent('You have logged in successfully! Enjoy!.')
                            .ariaLabel('Success')
                            .ok('Ok')
                            .targetEvent(ev)
                       );
                }
            });
    };
    //Logout func
    $scope.logout = function (ev) {
        firebase.auth().signOut()
            .then(function (success) {
                debugger;
                var successs = success;
                $mdDialog.show(
                               $mdDialog.alert()
                               .parent(angular.element(document.querySelector('#popupContainer')))
                               .clickOutsideToClose(true)
                               .title('Logout Success!')
                               .textContent('Goodbye!')
                               .ariaLabel('Success')
                               .ok('Ok')
                               .targetEvent(ev)
                          );
            }, function (error) {
                $mdDialog.show(
                               $mdDialog.alert()
                               .parent(angular.element(document.querySelector('#popupContainer')))
                               .clickOutsideToClose(true)
                               .title(error.code)
                               .textContent(error.message)
                               .ariaLabel('Success')
                               .ok('Ok')
                               .targetEvent(ev)
                          );
            });
    };

});