﻿// Initialize Firebase

var config = {
    apiKey: "AIzaSyBG06POSZ2EWCMm1gnWiVxIVcwrXwW8B8I",
    authDomain: "myfirebase-babea.firebaseapp.com",
    databaseURL: "https://myfirebase-babea.firebaseio.com",
    storageBucket: "myfirebase-babea.appspot.com",
};
firebase.initializeApp(config);


var database = firebase.database();
var storage = firebase.storage();

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        userData = user;
        $('#id-login-section, #id-signup-section').hide();
        $('#id-login-section, #id-signup-section').disabled();
    } else {
        userData = undefined;
    }
});
