﻿app.directive('myFile', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.myFile);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.controller('uploadFileCtrl', function ($scope, $mdDialog, $mdMedia, $timeout, $sce) {

    $scope.submitInfo = function (ev) {

    };

    $scope.uploadmusic = function (ev) {

        var file = $scope.mp3;

        var metadata = { contentType: file.type, };
        var uploadTask = storage.ref().child('songs/' + file.name).put(file, metadata);

        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                function (snapshot) {
                    // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                    $timeout(function () {
                        $scope.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                        //If progess == 100 then reset progess percentage = 0 after 1500 ms
                        if ($scope.progress == 100) {
                            $timeout(function () { $scope.progress = 0; }, 1500);
                        }
                    });
                    switch (snapshot.state) {
                        case firebase.storage.TaskState.PAUSED: // or 'paused'
                            console.log('Upload is paused');
                            break;
                        case firebase.storage.TaskState.RUNNING: // or 'running'
                            console.log('Upload is running');
                            break;
                    }
                },
                function (error) {
                    switch (error.code) {
                        case 'storage/unauthorized':
                            $mdDialog.show(
                              $mdDialog.alert()
                              .parent(angular.element(document.querySelector('#popupContainer')))
                              .clickOutsideToClose(true)
                              .title('Unauthorized!')
                              .textContent(error.message)
                              .ariaLabel('Error')
                              .ok('Got it!')
                              .targetEvent(ev));
                            break;

                        case 'storage/canceled':
                            $mdDialog.show(
                              $mdDialog.alert()
                              .parent(angular.element(document.querySelector('#popupContainer')))
                              .clickOutsideToClose(true)
                              .title('Upload canceled')
                              .textContent(error.message)
                              .ariaLabel('Error')
                              .ok('Got it!')
                              .targetEvent(ev));
                            break;

                        case 'storage/unknown':
                            $mdDialog.show(
                              $mdDialog.alert()
                              .parent(angular.element(document.querySelector('#popupContainer')))
                              .clickOutsideToClose(true)
                              .title('Unknow error')
                              .textContent(error.message)
                              .ariaLabel('Error')
                              .ok('Got it!')
                              .targetEvent(ev));
                            break;
                    }
                },
                function () {
                    // Upload completed successfully, now we can get the download URL
                    var downloadURL = uploadTask.snapshot.downloadURL;

                    var songInfo = {
                        fileName: file.name,
                        fileUrl: downloadURL,
                        uploader: userData.uid
                    };
                    var songInfoKey = database.ref().child('songs').push().key;

                    var updates = {};

                    updates['/songsInfo/' + songInfoKey] = songInfo

                    database.ref().update(updates)
                        .then(
                        function (success) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                .parent(angular.element(document.querySelector('#popupContainer')))
                                .clickOutsideToClose(true)
                                .title('Success!')
                                .textContent(downloadURL)
                                .ariaLabel('Success')
                                .ok('Got it!')
                                .targetEvent(ev));
                        },
                        function (error) {
                            storage.ref().child('songs/' + file.name).delete()
                                .then(
                                function (success) {
                                    $mdDialog.show(
                                        $mdDialog.alert()
                                        .parent(angular.element(document.querySelector('#popupContainer')))
                                        .clickOutsideToClose(true)
                                        .title('Error!')
                                        .textContent(error.message)
                                        .ariaLabel('Error')
                                        .ok('Got it!')
                                        .targetEvent(ev));
                                },
                                function (error) {
                                    $mdDialog.show(
                                        $mdDialog.alert()
                                        .parent(angular.element(document.querySelector('#popupContainer')))
                                        .clickOutsideToClose(true)
                                        .title('Error!')
                                        .textContent(error.message)
                                        .ariaLabel('Error')
                                        .ok('Got it!')
                                        .targetEvent(ev));
                                });
                        }
                        );

                });
    };

    $scope.loadSongs = function (ev) {
        var songsRef = database.ref('/songsInfo').once('value').then(function (dataSnapshot) {
            $timeout(function () {
                $scope.songs = dataSnapshot.val();
                songs = $scope.songs;
            });
        });
    };

    $scope.trustURL = function (songInf) {
        $('#id-song-source').attr('src', $sce.trustAsResourceUrl(songInf.fileUrl));
        $('#id-player')[0].load();
    }
}).config(function ($mdThemingProvider) {

    $mdThemingProvider.theme('docs-dark', 'default').primaryPalette('blue').dark();

});