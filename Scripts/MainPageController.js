﻿app.controller('mainPageCtrl', function ($scope, $mdDialog, $mdMedia, $timeout, $sce, $mdSidenav) {

    $scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');
    function buildToggler(componentId) {
        return function () {
            $mdSidenav(componentId).toggle();
        }
    }
});